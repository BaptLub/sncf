﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;

namespace FormationSNCF.Vues
{
    public partial class FormGestionLieux : Form
    {
        public FormGestionLieux()
        {
            InitializeComponent();
        }

        private void FormGestionLieu_Load(object sender, EventArgs e)
        {
            dataGridViewListeLieu.DataSource = Donnees.CollectionLieu;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void buttonAjoutLieu_Click(object sender, EventArgs e)
        {
            if (textBoxLibelle.Text.Length < 3)
            {
                MessageBox.Show("Le lieu doit être composé d'au moins 3 caractères");
                textBoxLibelle.Text = "";
                textBoxLibelle.Focus();
            }
            else if (textBoxTelephone.Text.Length == 0)
            {
                MessageBox.Show("Le numéro de téléphone est obligatoire");
                textBoxTelephone.Focus();
            }
            else if (textBoxCP.Text.Length == 0)
            {
                MessageBox.Show("Le code postal est obligatoire");
                textBoxCP.Focus();
            }
            else
            {
                dataGridViewListeLieu.DataSource = null;
                int numeroMax = 0;
                foreach (Lieu lieuCourant in Donnees.CollectionLieu)
                {
                    if (lieuCourant.Numero > numeroMax)
                        numeroMax = lieuCourant.Numero;
                }
                Lieu unLieu = new Lieu(numeroMax + 1, textBoxLibelle.Text, textBoxCP.Text, textBoxTelephone.Text);
                Donnees.CollectionLieu.Add(unLieu);
                dataGridViewListeLieu.DataSource = Donnees.CollectionLieu;
            }
        }
        private void TextBoxCP_Leave(object sender, EventArgs e)
        {
            if (textBoxCP.Text.Length != 0)
            {
                if (!Formulaire.VerificationFormatCodePostal(textBoxCP.Text))
                {
                    MessageBox.Show("Saisir un code postal à 5 chiffres");
                    textBoxCP.Text = "";
                    textBoxCP.Focus();
                }
            }
        }
        private void TextBoxTelephone_Leave(object sender, EventArgs e)
        {
            if (textBoxTelephone.Text != "")
            {
                if (!Formulaire.VerificationTelephone(textBoxTelephone.Text))
                {
                    MessageBox.Show("Saisir un numéro de téléphone à 10 chiffres");
                    textBoxTelephone.Text = "";
                    textBoxTelephone.Focus();
                }
            }
        }
        private void GestionDesLieuxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormGestionLieux();
        }
        private void FormGestionLieux_Load(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public FormGestionLieux MdiChild { get; set; }
    }
}
