﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;

namespace FormationSNCF.Vues
{
    public partial class FormFenetrePrincipale : Form
    {
        private Form _mdiChild;

        private Form MdiChild
        {
            get { return _mdiChild;  }
            set
            {

                if (_mdiChild != null)
                {
                    _mdiChild.Dispose();
                }
                _mdiChild = value;
                _mdiChild.MdiParent = this;
                _mdiChild.MaximumSize = _mdiChild.Size;
                _mdiChild.MinimumSize = _mdiChild.Size;
                _mdiChild.Show();

            }
        }
        
        public FormFenetrePrincipale()
        {
            InitializeComponent();
        }

        private void GestionDesLieuxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormGestionLieux();
        }

        private void FormFenetrePrincipaleForm_Closing(object sender, FormClosingEventArgs e)
        {
            Donnees.SauvegardeDonnees();
        }
        private void GestionDesActionsDeFormationsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormGestionActionFormation();
        }

        
    }
}
