﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Ressources;

namespace FormationSNCF.Vues
{
    public partial class FormGestionActivite : Form
    {
        public FormGestionActivite()
        {                       
            InitializeComponent();
        }

        private void buttonAjoutActivite_Click(object sender, EventArgs e)
        {
            if (textBoxNomActivite.Text.Length < 3)
            {
                MessageBox.Show("l'activité doit être composé d'au moins 3 caractères");
                textBoxNomActivite.Text = "";
                textBoxNomActivite.Focus();
            }
            
            
            
            Activite uneActivite = new Activite(textBoxNomActivite.Text);
            if (Donnees.CollectionActivite.Contains(uneActivite) == false)
            {
                Donnees.CollectionActivite.Add(uneActivite);
            }

            else
            {
                MessageBox.Show("existe déja");
            }
            //    bool contenirActivite = false;
            //    if (listBoxListeActivites.Contains(textBoxNomActivite))
            //    {
            //        contenirActivite = true;
            //    }
            //}
            //Activite uneActivite = new Activite(textBoxNomActivite.Text);
            //Donnees.CollectionActivite.Add(uneActivite);
           listBoxListeActivites.DataSource = Donnees.CollectionActivite;

        }
    }
}
