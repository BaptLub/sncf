﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using FormationSNCF.Modele;
using FormationSNCF.Vues;
using FormationSNCF.Ressources;
using System.Text.RegularExpressions;


namespace FormationSNCF.Vues
{
    public partial class FormAjouterAgent : Form
    {

        private FormAjouterAgent MdiChild;
        public FormAjouterAgent()
        {
            InitializeComponent();
            MinimumSize = Size;
            MaximumSize = Size;
        }

        private bool VerifFormulaireAjoutAgent()
        {
            bool resultat = false;

            if (this.comboBoxCivilite.SelectedIndex <= 0)
            {
                MessageBox.Show("Sélectionner la civilité de l'agent");
            }
            else if (this.textBoxNom.Text == "")
            {
                MessageBox.Show("Saisir le nom de l'agent");
            }
            else if (this.textBoxPrenom.Text == "")
            {
                MessageBox.Show("Saisir le prénom de l'agent");
            }
            else
            {
                resultat = true;
            }
            return resultat;
        }

        private void TextBoxCodePostal_Leave(object sender, EventArgs e)
        {
            if (textBoxCodePostal.Text.Length != 0)
            {
                if (!Formulaire.VerificationFormatCodePostal(textBoxCodePostal.Text))
                {
                    MessageBox.Show("Saisir un code postal à 5 chiffres");
                    textBoxCodePostal.Text = "";
                    textBoxCodePostal.Focus();
                }
            }
        }

        private void TextBoxDateNaissance_Leave(object sender, EventArgs e)
        {
            if (textBoxDateNaissance.Text.Length != 0)
            {
                if (!Formulaire.VerificationFormatDate(textBoxDateNaissance.Text))
                {
                    MessageBox.Show("Format : jj-mm-aaaa ou jj/mm/aaaa");
                    textBoxDateNaissance.Text = "";
                    textBoxDateNaissance.Focus();
                }
            }
        }

        private void TextBoxDateEmbauche_Leave(object sender, EventArgs e)
        {
            if (textBoxDateNaissance.Text.Length != 0)
            {
                if (!Formulaire.VerificationFormatDate(textBoxDateEmbauche.Text))
                {
                    MessageBox.Show("Format : jj-mm-aaaa ou jj/mm/aaaa");
                    textBoxDateEmbauche.Text = "";
                    textBoxDateEmbauche.Focus();
                }
            }
        }
        private void AjouterUnAgentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MdiChild = new FormAjouterAgent();
        }

        private void FormAjoutAgent_Load(object sender, EventArgs e)
        {
            comboBoxLieu.DataSource = Donnees.CollectionLieu;
            comboBoxLieu.DisplayMember = "LibelleLieu";
            int max = 0;
            foreach (Agent agentCourant in Donnees.CollectionAgent)
            {
                if (agentCourant.Numero > max)
                    max = agentCourant.Numero;
            }
            labelNumeroAgent.Text = (max + 1).ToString();
        }
        private void ReinitialiserFormulaire()
        {
            foreach (Control controlCourant in ajoutAgentGroupBox.Controls)
            {
                if (controlCourant.GetType() == typeof(TextBox))
                {
                    (controlCourant as TextBox).Clear();
                }
            }
            int max = 0;
            foreach (Agent agentCourant in Donnees.CollectionAgent)
            {
                if (agentCourant.Numero > max)
                    max = agentCourant.Numero;
            }
            labelNumeroAgent.Text = (max + 1).ToString();

            comboBoxCivilite.SelectedIndex = 0;
        }
        private void ButtonAjoutAgent_Click(object sender, EventArgs e)
        {
            int numeroAgent = int.Parse(labelNumeroAgent.Text);
            string civilite = comboBoxCivilite.Text;
            string nomAgent = textBoxNom.Text;
            string prenomAgent = textBoxPrenom.Text;
            Agent unAgent = new Agent(numeroAgent, civilite, prenomAgent, nomAgent);

            unAgent.Adresse1 = textBoxAdresse1.Text;
            unAgent.Adresse2 = textBoxAdresse2.Text;
            unAgent.CodePostal = textBoxCodePostal.Text;
            unAgent.Ville = textBoxVille.Text;
            if (textBoxDateNaissance.Text != "")
                unAgent.DateNaissance = Convert.ToDateTime(textBoxDateNaissance.Text);
            if (textBoxDateEmbauche.Text != "")
                unAgent.DateEmbauche = Convert.ToDateTime(textBoxDateEmbauche.Text);


            if (comboBoxLieu.SelectedIndex != -1)
            {
                Lieu unLieu = null;
                foreach (Lieu lieu in Donnees.CollectionLieu)
                {
                    if(comboBoxLieu.Text == lieu.LibelleLieu)
                    {
                        unLieu = lieu;
                        break;
                    }
                }
                unAgent.LieuDeTravaille = unLieu;
            }

            Donnees.CollectionAgent.Add(unAgent);

            ReinitialiserFormulaire();
        }
    }
}