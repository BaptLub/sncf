﻿using FormationSNCF.Modele;
using FormationSNCF.Ressources;

namespace FormationSNCF.Vues
{
    partial class FormFenetrePrincipale
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormFenetrePrincipale));
            this.menuPrincipalMenuStrip = new System.Windows.Forms.MenuStrip();
            this.lieuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionDesLieuxToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuPrincipalMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPrincipalMenuStrip
            // 
            this.menuPrincipalMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.lieuToolStripMenuItem});
            this.menuPrincipalMenuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuPrincipalMenuStrip.Name = "menuPrincipalMenuStrip";
            this.menuPrincipalMenuStrip.Size = new System.Drawing.Size(284, 24);
            this.menuPrincipalMenuStrip.TabIndex = 1;
            this.menuPrincipalMenuStrip.Text = "menuStrip1";
            // 
            // lieuToolStripMenuItem
            // 
            this.lieuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gestionDesLieuxToolStripMenuItem});
            this.lieuToolStripMenuItem.Name = "lieuToolStripMenuItem";
            this.lieuToolStripMenuItem.Size = new System.Drawing.Size(49, 20);
            this.lieuToolStripMenuItem.Text = "LIEUX";
            // 
            // gestionDesLieuxToolStripMenuItem
            // 
            this.gestionDesLieuxToolStripMenuItem.Name = "gestionDesLieuxToolStripMenuItem";
            this.gestionDesLieuxToolStripMenuItem.Size = new System.Drawing.Size(178, 22);
            this.gestionDesLieuxToolStripMenuItem.Text = "GESTION DES LIEUX";
            this.gestionDesLieuxToolStripMenuItem.Click += new System.EventHandler(this.GestionDesLieuxToolStripMenuItem_Click);
            // 
            // FormFenetrePrincipale
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.menuPrincipalMenuStrip);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuPrincipalMenuStrip;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormFenetrePrincipale";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormationSNCF";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuPrincipalMenuStrip.ResumeLayout(false);
            this.menuPrincipalMenuStrip.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuPrincipalMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem lieuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionDesLieuxToolStripMenuItem;
    }
}