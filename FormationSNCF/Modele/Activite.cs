﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;


namespace FormationSNCF.Modele
{
    [Serializable]
    /// <summary>
    /// Classe métier : représente une activité
    /// </summary>
    public class Activite
    {
        private string _libelleActivite;
        private List<ActionFormation> _lesActionsFormation;

        /// <summary>
        /// Obtient le libellé de l'activité
        /// </summary>
        public string LibelleActivite
        {
            get { return _libelleActivite; }
        }
        
        /// <summary>
        /// Initialise une activité
        /// </summary>
        /// <param name="libelleActivite">Le libellé de l'activité</param>
        public Activite(string libelleActivite)
        {
            _libelleActivite = libelleActivite;
            _lesActionsFormation = new List<ActionFormation>();
        }
        public void AjouterActionFormation(string unCodeAction, string unIntituleDeFormation, decimal unCout, int uneDuree, DateTime uneDate)
        {
            ActionFormation uneActionDeFormation = new ActionFormation(unCodeAction, unIntituleDeFormation, unCout, uneDuree, uneDate);
            _lesActionsFormation.Add(uneActionDeFormation);
        }
        public ActionFormation ObtenirActionFormation (string unCodeAction)
        {
            ActionFormation actionFormation = null;
            foreach (ActionFormation actionFormationCourante in _lesActionsFormation)
            {
                if (actionFormationCourante.CodeAction == unCodeAction)
                {
                    actionFormation = actionFormationCourante;
                    break;
                }
            }
            return actionFormation;
        }
        public bool SupprimerActionFormation(string codeAction)
        {
            return _lesActionsFormation.Remove(ObtenirActionFormation(codeAction));
        }
        public ReadOnlyCollection<ActionFormation> ActionFormation
        {
            get
            {
                return new ReadOnlyCollection<ActionFormation>(_lesActionsFormation);
            }
        }
        public override bool Equals(object uneActivité)
        {
            bool egal = false;
            Activite activiteComparee = uneActivité as Activite;
            if (activiteComparee != null)
            {
                if (activiteComparee.LibelleActivite == _libelleActivite)
                    egal = true;
            }
            return egal;
        }
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
        public override string ToString()
        {
            return _libelleActivite;
        }
    }
}
